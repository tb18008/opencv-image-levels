//
// Created by User on 6/13/2021.
//

#ifndef IMAGE_LEVELS_GRID_LAYOUT_H
#define IMAGE_LEVELS_GRID_LAYOUT_H

#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include "four_sided.h"

using namespace std;
using namespace cv;

class GridLayout {

    struct GridElement{
        Mat element_image;
        Rect element_rect;

        bool empty() const{
            return element_image.empty();
        };
    };

    private:

    unsigned char cols_count;
    unsigned char rows_count;
    int* column_widths;
    int* row_heights;
    GridElement** grid_elements;
    Scalar background_color;

    void centerImages();
    static void wrapImage(Mat& img, const Scalar& color, FourSided gaps);

    public:

        explicit GridLayout(unsigned char cols = 1, unsigned char rows = 1, const Scalar& background_color = Scalar(255, 255, 255));
        bool addImage(Mat& img, const Scalar& border_color = Scalar(0, 0, 0), FourSided paddings = FourSided(0), FourSided borders = FourSided(1), FourSided margins = FourSided(5));
        void drawImage(const string& window_name);
        void clearImages();

};


#endif //IMAGE_LEVELS_GRID_LAYOUT_H
