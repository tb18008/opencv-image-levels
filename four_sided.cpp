//
// Created by User on 6/15/2021.
//

#include "four_sided.h"

FourSided::FourSided(unsigned int all) {
    this->left = all;
    this->top = all;
    this->right = all;
    this->bottom = all;

    x = y = all * 2;
}

FourSided::FourSided(unsigned int x, unsigned int y) {
    this->left = x;
    this->top = y;
    this->right = x;
    this->bottom = y;

    this->x = x;
    this->y = y;
}

FourSided::FourSided(unsigned int left, unsigned int top, unsigned int right, unsigned int bottom) {
    this->left = left;
    this->top = top;
    this->right = right;
    this->bottom = bottom;

    x = left + right;
    y = top + bottom;
}
