//
// Created by User on 5/23/2021.
//

#ifndef IMAGE_LEVELS_IMAGE_LEVELS_H
#define IMAGE_LEVELS_IMAGE_LEVELS_H

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include "grid_layout.h"
#include "four_sided.h"

using namespace std;
using namespace cv;

enum ChannelTypes {
    RED_CHANNEL  = 2,
    GREEN_CHANNEL  = 1,
    BLUE_CHANNEL  = 0
};

class ImageLevels {

    struct Histogram{
        int values[256][3];
        int most_often[3];
        bool normalized = false;
    };

    private:

        //Variables

        Histogram original_img_histogram;
        Histogram distribution_function;
        Histogram leveled_img_histogram;

        Mat original_img;
        Mat leveled_img;

        Scalar background_color = Scalar(0,0,0);
        Scalar border_color = Scalar(255,255,255);

        int current_channel = RED_CHANNEL;

        //Functions
        void draw();
        void drawHistogram(Histogram histogram);
        void drawLabel(const string& label_text, const Rect& label_rect, FourSided margins);
        void calculateDistribution();
        void normalize(Histogram& histogram) const;

        GridLayout grid_layout = GridLayout(3,3,background_color);
//        GridLayout grid_layout = GridLayout(3,2,background_color);

        void changeChannel(int channel_enum);

    public:

        //Variables

        //Constructors
        explicit ImageLevels();
        explicit ImageLevels(Mat &img_before);

        //Functions
        void waitUserInput();
};


#endif //IMAGE_LEVELS_IMAGE_LEVELS_H
