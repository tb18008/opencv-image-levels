//
// Created by User on 5/23/2021.
//

#include "image_levels.h"

ImageLevels::ImageLevels(Mat &img_before) {

    //Zero out all the values for the counters
    for (int i = 0; i < 256; ++i) {
        for (int channel = 0; channel < 3; ++channel) {
            original_img_histogram.values[i][channel] = 0;
            original_img_histogram.most_often[channel] = 0;
            distribution_function.most_often[channel] = 0;
            leveled_img_histogram.values[i][channel] = 0;
            leveled_img_histogram.most_often[channel] = 0;
        }
    }

    original_img = img_before;
    leveled_img = original_img;

    //Count pixel value frequency of original image
    for (int x = 0; x < original_img.cols; ++x) {
        for (int y = 0; y < original_img.rows; ++y) {
            for (int channel = 0; channel < 3; ++channel) {
                ++ original_img_histogram.values[(int)original_img.ptr(y, x)[channel]][channel];
            }
        }
    }

    calculateDistribution();

    //Get highest value for each channel
    for (int i = 0; i < 256; ++i) {
        for (int channel = 0; channel < 3; ++channel) {
            if(original_img_histogram.most_often[channel] < original_img_histogram.values[i][channel]) original_img_histogram.most_often[channel] = original_img_histogram.values[i][channel];
            if(distribution_function.most_often[channel] < distribution_function.values[i][channel]) distribution_function.most_often[channel] = distribution_function.values[i][channel];
        }
    }

    normalize(distribution_function);

    //Apply distribution function
    unsigned char * pixel;
    for (int x = 0; x < leveled_img.cols; ++x) {
        for (int y = 0; y < leveled_img.rows; ++y) {
            pixel = leveled_img.ptr(y, x);
            for (int channel = 0; channel < 3; ++channel) {
                pixel[channel] = distribution_function.values[leveled_img.ptr(y, x)[channel]][channel];
            }
        }
    }
    imshow("Leveled image", leveled_img);

    //Count pixel value frequency of leveled image
    for (int x = 0; x < leveled_img.cols; ++x) {
        for (int y = 0; y < leveled_img.rows; ++y) {
            for (int channel = 0; channel < 3; ++channel) {
                ++ leveled_img_histogram.values[(int)leveled_img.ptr(y, x)[channel]][channel];
            }
        }
    }

    //Get highest value for each channel
    for (int i = 0; i < 256; ++i) {
        for (int channel = 0; channel < 3; ++channel) {
            if(leveled_img_histogram.most_often[channel] < leveled_img_histogram.values[i][channel]) leveled_img_histogram.most_often[channel] = leveled_img_histogram.values[i][channel];
        }
    }

    draw();

}

void ImageLevels::calculateDistribution() {

    //Calculate distribution for value 0
    for (int channel = 0; channel < 3; ++channel) {
        distribution_function.values[0][channel] = original_img_histogram.values[0][channel];
    }

    //Calculate distribution for rest of the values
    for (int i = 1; i < 256; ++i) {
        for (int channel = 0; channel < 3; ++channel) {
            distribution_function.values[i][channel] = distribution_function.values[i - 1][channel];
            distribution_function.values[i][channel] += original_img_histogram.values[i][channel];
        }
    }
}


void ImageLevels::draw() {

    drawLabel("Original histogram",Rect(0,0,256,25),FourSided(5));
    drawHistogram(original_img_histogram);
    drawLabel(" ",Rect(0,0,1,1),FourSided());
    drawLabel("Distribution function",Rect(0,0,256,25),FourSided(5));

    drawHistogram(distribution_function);
    const char *channel_names[3] = { "Blue", "Green", "Red" };
    string button_text = "Channel: ";
    button_text += channel_names[current_channel];
    drawLabel(button_text,Rect(0,0,256,25),FourSided(5));

    drawLabel("Leveled image histogram",Rect(0,0,256,25),FourSided(5));
    drawHistogram(leveled_img_histogram);
    drawLabel(" ",Rect(0,0,1,1),FourSided());

    grid_layout.drawImage("Histogram");
}

void ImageLevels::drawHistogram(Histogram histogram) {

    Mat graph_before(256,256, CV_8UC3, Scalar(127, 127, 127));

    normalize(histogram);

    //Draw lines of the graph
    for (int i = 0; i < 256; ++i) {
        line(graph_before,
             Point(i, 256),
             Point(i, 256 - histogram.values[i][current_channel]),
             Scalar(i * (0 == current_channel),i * (1 == current_channel), i * (2 == current_channel)), 1, LINE_4);
    }

    grid_layout.addImage(graph_before, border_color);
}


ImageLevels::ImageLevels() {

}

void ImageLevels::waitUserInput() {
    //Infinite cycle until user presses x key
    bool exit = false;
    while (!exit) {
        int keystroke = waitKey(0); // Wait for a keystroke in the window
        switch (keystroke) {
            case 'r':
                changeChannel(RED_CHANNEL);
                break;
            case 'g':
                changeChannel(GREEN_CHANNEL);
                break;
            case 'b':
                changeChannel(BLUE_CHANNEL);
                break;
            case 27:    // Esc key pressed
                exit = true;
            default:
                break;
        }
    }
}

void ImageLevels::changeChannel(int channel_enum) {
    current_channel = channel_enum;
    grid_layout.clearImages();
    draw();
}

void ImageLevels::normalize(ImageLevels::Histogram& histogram) const {

    if(histogram.normalized) return;

    for (auto & value : histogram.values) {
        for (int channel = 0; channel < 3; ++channel) {
            value[channel] = 255 * (float)value[channel] / histogram.most_often[channel];
        }
    }

    histogram.normalized = true;
}

void ImageLevels::drawLabel(const string &label_text, const Rect& label_rect, FourSided margins) {

    //Draw label text
    float font_scale = 1;

    Rect text_rect = label_rect;
    text_rect.x += margins.left;
    text_rect.y += margins.top;
    text_rect.width -= margins.right + margins.left;
    text_rect.height -= margins.bottom + margins.top;

    Point text_position = Point(text_rect.tl().x + text_rect.width / 2, text_rect.tl().y + text_rect.height / 2);

    //Get computed text size to determine if it fits the button
    Size text_size = getTextSize(label_text,FONT_HERSHEY_SIMPLEX, font_scale, 1, nullptr);

    //If text is too high or too wide decrease the scale
    if(text_size.height > text_rect.height){
        font_scale = text_rect.height * font_scale / text_size.height;
        font_scale = round(font_scale * 10 - 0.5)/10;
        text_size = getTextSize(label_text,FONT_HERSHEY_SIMPLEX, font_scale, 1, nullptr);
    }
    else if(text_size.width > text_rect.width){
        font_scale = text_rect.width * font_scale / text_size.width;
        font_scale = round(font_scale * 10 - 0.5)/10;
        text_size = getTextSize(label_text,FONT_HERSHEY_SIMPLEX, font_scale, 1, nullptr);
    }

    //Get position of text (centered inside button) keeping text size in mind
    text_position.x = text_rect.x + text_rect.width / 2 - text_size.width / 2;
    text_position.y = text_rect.y + text_rect.height / 2 + text_size.height / 2;

    Scalar black = Scalar(0, 0, 0);

    Mat label_img = Mat(label_rect.height, label_rect.width, CV_8UC3, Scalar(0, 0, 0));

    putText(label_img, label_text, text_position, FONT_HERSHEY_SIMPLEX, font_scale, Scalar(255, 255, 255), 1, LINE_AA);

    grid_layout.addImage(label_img, black);

}

