//
// Created by User on 6/13/2021.
//

#include "grid_layout.h"

GridLayout::GridLayout(unsigned char cols, unsigned char rows, const Scalar& background_color) {
    this->cols_count = cols;
    this->rows_count = rows;

    //Initialize array of images
    grid_elements = new GridElement*[cols_count];
    column_widths = new int[cols_count];
    for(int i = 0; i < cols_count; ++i) {
        grid_elements[i] = new GridElement[rows_count];
        row_heights = new int[rows_count];
    }

    for (int i = 0; i < (int)cols_count; ++i) column_widths[i] = 0;
    for (int i = 0; i < (int)rows_count; ++i) row_heights[i] = 0;

    this->background_color = background_color;
}

bool GridLayout::addImage(Mat &img, const Scalar& border_color, FourSided paddings, FourSided borders, FourSided margins) {

    //Find last empty spot for image and add
    for (int x = 0; x < cols_count; ++x) {
        for (int y = 0; y < rows_count; ++y) {
            if(grid_elements[x][y].empty()) {

                //Wrap image in paddings
                wrapImage(img,background_color,paddings);
                //Wrap image in borders
                wrapImage(img,border_color,borders);
                //Wrap image in margins
                wrapImage(img,background_color,margins);

                //Add image and rectangle
                grid_elements[x][y].element_image = img;

                //Increase column widths or row heights
                if(img.cols > column_widths[x]) column_widths[x] = img.cols;
                if(img.rows > row_heights[y]) row_heights[y] = img.rows;

                return true;
            }
        }
    }

    return false;
}

void GridLayout::drawImage(const string &window_name) {

    //Don't draw if empty
    if(grid_elements[0][0].empty()) return;

    centerImages();

    //Calculate total window size

    Rect window_size = Rect(0, 0, 0,0);

    for (int i = 0; i < (int)cols_count; ++i) window_size.x += column_widths[i];
    for (int i = 0; i < (int)rows_count; ++i) window_size.y += row_heights[i];

    Mat window(window_size.y,window_size.x, CV_8UC3, background_color);

    Mat img;
    Rect rect;

    //Copy images to window image
    for (int x = 0; x < cols_count; ++x) {
        for (int y = 0; y < rows_count; ++y) {

            if(grid_elements[x][y].empty()) break;

            img = grid_elements[x][y].element_image;
            rect = grid_elements[x][y].element_rect;

            img.copyTo(window(rect));
        }
    }

    imshow(window_name, window);
}

void GridLayout::centerImages() {

    Mat img;
    Rect rect;

    Point image_position;

    //Align elements
    for (int x = 0; x < cols_count; ++x) {
        for (int y = 0; y < rows_count; ++y) {

            if(grid_elements[x][y].empty()) return;

            img = grid_elements[x][y].element_image;
            grid_elements[x][y].element_rect.width = column_widths[x];
            grid_elements[x][y].element_rect.height = row_heights[y];

            //Image position is cell center minus image center
            image_position = Point(column_widths[x] / 2, row_heights[y] / 2) - Point(img.cols / 2, img.rows / 2);

            if(x == 0 && y == 0) {  //First element
                rect = Rect(0, 0, img.cols, img.rows);
            }
            else if(y == 0){    //Element in new column
                rect = Rect(0, 0, img.cols, img.rows);
                rect.x = grid_elements[x - 1][y].element_rect.x + column_widths[x - 1]; //Move right by one column
                rect.y = 0;
            }
            else{   //Element below last element
                rect = Rect(0, 0, img.cols, img.rows);
                rect.x = grid_elements[x][y - 1].element_rect.x; //Move to the right by one column
                rect.y = grid_elements[x][y - 1].element_rect.y + row_heights[y-1];

            }

            //Center the image
            rect.x += image_position.x;
            rect.y += image_position.y;

            grid_elements[x][y].element_rect = rect;
        }
    }
}

void GridLayout::wrapImage(Mat& img, const Scalar &color, FourSided gaps) {
    Rect image_rect = Rect(gaps.left, gaps.top, img.cols, img.rows);

    Mat image_wrapped = Mat(img.rows + gaps.y, img.cols + gaps.x,CV_8UC3, color);
    img.copyTo(image_wrapped(image_rect));

    img = image_wrapped;
}

void GridLayout::clearImages() {
    //Find last empty spot for image and add
    for (int x = 0; x < cols_count; ++x) {
        for (int y = 0; y < rows_count; ++y) {
            //Add image and rectangle
            grid_elements[x][y].element_image = Mat(0,0,CV_8UC3);
        }
    }
}
