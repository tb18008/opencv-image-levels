#include <iostream>
#include "image_levels.h"

using namespace std;
using namespace cv;

string file_path;

ImageLevels image_levels;

int main(int argc, char *argv[]) {
    //    Checks whether user has started executable with an picture_image in the arguments
    if (argc > 1) file_path = argv[1];
    else
    {
        cout << "Selected picture_image file path: ";
        getline (cin, file_path);
    }

    cout << "\tImage leveling\n" << endl;
    cout << "\tAutomatically applies distribution function to original image\n" << endl;
    cout << "\tControls:" << endl;
    cout << "\t - 'r', 'g' and 'b' changes channel shown in histograms;" << endl;
    cout << "\t - 'Esc' to Quit." << endl;

    Mat original_img = imread(file_path);   //Read file

    imshow("Original image", original_img); //Show original image

    image_levels = ImageLevels(original_img);   //Create ImageLevels object

    image_levels.waitUserInput();   //Loop until user presses Esc

    return 0;
}
