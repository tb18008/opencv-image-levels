//
// Created by User on 6/15/2021.
//

#ifndef IMAGE_LEVELS_FOUR_SIDED_H
#define IMAGE_LEVELS_FOUR_SIDED_H


class FourSided {
    public:
        unsigned int left;
        unsigned int top;
        unsigned int right;
        unsigned int bottom;
        unsigned int x;
        unsigned int y;
        explicit FourSided(unsigned int all = 0);
        explicit FourSided(unsigned int x, unsigned int y);
        explicit FourSided(unsigned int left, unsigned int top, unsigned int right, unsigned int bottom);
};


#endif //IMAGE_LEVELS_FOUR_SIDED_H
